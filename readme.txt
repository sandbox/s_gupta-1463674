CONTENTS OF THE FILE
--------------------
  * Description
  * Installation and configuration
  
DESCRIPTION
-----------
Active menu link or aml is a module that allows a particular menu link to
active on desired path Desired paths can be expressed in the same manner as one 
does in the 'visibility configuration' of Drupal Blocks. 

INSTALLATION AND CONFIGURATION
------------------------------
  * Copy active menu link folder in your sities all module directory
  * Enable the module by admin/modules or through drush command "Drush en aml"
  * Go to admin/stucture/menus and then move to edit link page of the menu link
    you want to be active
  * On edit link page the aml configuration options are automatically added
  * Then you can choose the appropriate option and specify the path on which
    you want that menu item to be active on
    