(function ($) {
/**
 * Provides summary information for the aml settings vertical tabs
 */
  Drupal.behaviors.aml = {
    attach: function (context) {
	  if (typeof jQuery.fn.drupalSetSummary == 'undefined') {
	    return;
	  }
	  $('fieldset#edit-path', context).drupalSetSummary(function (context){
	  	if (!$('textarea[name = "pages"]', context).val()) {
	      return Drupal.t('Not restricted');
	    }
	    else {
	      return Drupal.t('Restricted to certain pages');
	    }
	  });
  	}
  }
})(jQuery);
